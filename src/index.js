import Player from "./player/Player";

let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

// creating a new phaser game class
new Phaser.Game(config);
// creating a graphics object
let graphics = null;

function preload() {
    this.load.spritesheet('player', './res/player.png', { frameWidth: 32, frameHeight: 48 });
    this.load.image('ground', './res/ground.png');
    this.load.image('tree', './res/tree.png');
}

// The player object
let player;
// The controlls object
let cursors;

// The Static Group responsible for housing the trees
var trees;
// The text that will be displayed once colliding with a tree
var chopText;

// Inventory Setup
let inventory = [];
// Iventory text that will be displayed whenever a certain key is pressed
let inventoryText;
// The variable that will control when to display the inventory screen
let inventoryActive = false;

// setting up a timer object so that a specific key doesn't read more than once for a specific amount of time
let timers = {
    // tab keypress timer
    tabTimer: 100
};

function create() {
    // displaying the ground image
    this.add.image(400, 300, 'ground');

    

    // creating the static group for trees
    trees = this.physics.add.staticGroup();
    // adding trees as children to the trees StaticGroup
    trees.create(400, 568, 'tree').setScale(1).refreshBody();
    trees.create(600, 400, 'tree');
    trees.create(250, 250, 'tree');
    trees.create(750, 220, 'tree');

    // initializing the player object
    player = new Player(this, 100, 450);

    // setting up controlls
    cursors = this.input.keyboard.addKeys({
        up: Phaser.Input.Keyboard.KeyCodes.W,
        down: Phaser.Input.Keyboard.KeyCodes.S,
        left: Phaser.Input.Keyboard.KeyCodes.A,
        right: Phaser.Input.Keyboard.KeyCodes.D,
        tab: Phaser.Input.Keyboard.KeyCodes.TAB,
        use: Phaser.Input.Keyboard.KeyCodes.E,
        ONE: Phaser.Input.Keyboard.KeyCodes.ONE,
        TWO: Phaser.Input.Keyboard.KeyCodes.TWO,
        THREE: Phaser.Input.Keyboard.KeyCodes.THREE,
        FOUR: Phaser.Input.Keyboard.KeyCodes.FOUR,
        FIVE: Phaser.Input.Keyboard.KeyCodes.FIVE,
        SIX: Phaser.Input.Keyboard.KeyCodes.SIX
    });

    // setting up text objects and setting their display values to false
    chopText = this.add.text(300, 16, `Press 'E' to chop`);
    chopText.visible = false;
    inventoryText = this.add.text(300, 16, `Pree 'tab' to close inventory`);
    inventoryText.visible = false;

    // setting up collision with the player and trees
    this.physics.add.collider(player.sprite, trees);

    // initializing the graphics object
    graphics = this.make.graphics({x: 0, y: 0, add: true});
    
    // drawing the hotbar to the screen
    drawHotbar();
}

function update() {
    // testing for tree collision detection and checking if the use key(E) is pressed, if so, delete a child
    this.physics.collide(player.sprite, trees, () => {
        if(cursors.use.isDown) {
            trees.children.iterate(function(child) {
                child.disableBody();
                child.visible = false;
            });
        }
    }, null, this);

    // beginning the tabTimer
    timers.tabTimer -= 5;
    // making sure the tabTimer doesn't go below zero
    if(timers.tabTimer <= 0 && inventoryActive == false) {
        timers.tabTimer = 0;
        // checking for TAB keypress then displaying the inventory screen
        if(cursors.tab.isDown) {
            // Setting the inventoryActive variable to true, causing the inventory screen to appear
            inventoryActive = true;
            
            // Drawing the Inventory Screen
            drawInventoryScreen();

            // Setting tabTimer back to one-hundred
            timers.tabTimer = 100;
        }
    }
    // Checking if the tabTimer is less than or equal to zero and inventoryActive is equal to true, if so, run the following
    else if(timers.tabTimer <= 0 && inventoryActive == true) {
        timers.tabTimer = 0;
        // Checking if the player hits tab again; if so, hide the inventory screen
        if(cursors.tab.isDown ) {
            // Setting the inventoryActive variable to false, causing the inventory screen to disappear
            inventoryActive = false;
            
            // Clearing the graphics object
            graphics.clear();
            // re drawing the hotbar once it gets removed from the screen
            drawHotbar();

            // Setting tabTimer back to one-hundred
            timers.tabTimer = 100;
        }
    }

    // updating the player movement only if the inventory isn't open
    if(!inventoryActive) {
        player.updateMovement(cursors);
    }
}

// This function draws the inventory screen [TODO: Move to own class]
function drawInventoryScreen() {
     // drawing the backround
     graphics.beginPath();
     graphics.fillStyle(0x000000, 0.8);
     graphics.fillRect(16, 234, 768, 264);
     graphics.closePath();
     
     // drawing the squares 
     graphics.beginPath();
     graphics.fillStyle(0x444444, 1);
     for(let x = 0; x < 9; x++) {
         for(let y = 0; y < 3; y++) {
             graphics.fillRect(32+84*x, 234+16+84*y, 64, 64);
         }
     }
     graphics.closePath();
}

// This function draws the hotbar [TODO:Move to own class]
function drawHotbar() {
    // drawing the background
    graphics.beginPath();
    graphics.fillStyle(0x000000, 0.8);
    graphics.fillRect(150, 600-(64+32), 520, 64+32);
    graphics.closePath();

    // drawing the squares
    graphics.beginPath();
    graphics.fillStyle(0x444444, 1);
    for(let x = 0; x < 6; x++) {
        graphics.fillRect(150+16+84*x, 600-(64+16), 64, 64);
    }
}