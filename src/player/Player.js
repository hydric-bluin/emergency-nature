
export default class Player {

    constructor(scene, x, y) {

        this.sprite = scene.physics.add.sprite(x, y, 'player');
        this.sprite.setCollideWorldBounds(true);


    }

    updateMovement(cursors) {

        if (cursors.left.isDown) {
            this.sprite.setVelocityX(-250);
        } else if (cursors.right.isDown) {
            this.sprite.setVelocityX(250);
        } else {
            this.sprite.setVelocityX(0);
        }
        if (cursors.up.isDown) {
            this.sprite.setVelocityY(-250);
        } else if (cursors.down.isDown) {
            this.sprite.setVelocityY(250);
        } else {
            this.sprite.setVelocityY(0);
        }
    }


}
